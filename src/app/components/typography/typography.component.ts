import { AfterViewInit, Component, ElementRef, OnInit, ViewEncapsulation } from '@angular/core';
import Swiper, { EffectCoverflow } from 'swiper';
import { photoData, groupPhotos } from './typography.data'
import { PhotoModel } from './typography.models';
import SwiperCore, { Pagination, Navigation } from "swiper/core";

// install Swiper modules
SwiperCore.use([Pagination]);
@Component({
    selector: 'app-typography',
    templateUrl: './typography.component.html',
    styleUrls: ['./typography.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class TypographyComponent implements OnInit  , AfterViewInit {
  photosData: PhotoModel[] = photoData;
  groupPhotos: PhotoModel[] = groupPhotos;
  swiper: any;
  constructor( private elementRef: ElementRef) {
    this.photosData = photoData
    this.groupPhotos = groupPhotos;
  
   }
   ngAfterViewInit() {
    /**
     * - Here you are 100% sure, html is present on DOM, Swiper is initialize and is able to find your DOM element.
     * - Swiper constructor accept DOMElement as parameter, i recommand this approch to optimize DOM parsing.
     * - Because you store Swiper instance as attribute, you will be able to control Swiper by javascript. 
    **/
    this.swiper = new Swiper(this.elementRef.nativeElement.querySelector('.swiper-container'), {
        //Here you can provide Swiper config
    });
}
   onMouseOver(id: number){
    const index = this.photosData.findIndex(val => val.id === id);
    this.photosData[index].currentUrl = this.photosData[index].secondaryUrl
   }
   onMouseOut(id: number){
    const index = this.photosData.findIndex(val => val.id === id);
    this.photosData[index].currentUrl = this.photosData[index].principalUrl
   }
  ngOnInit() {}

}
