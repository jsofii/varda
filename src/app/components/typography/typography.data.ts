import { PhotoModel } from './typography.models'

export let photoData: PhotoModel[] = [
    {
        id: 1,
        name: "Gabriela Giacometti",
        principalUrl: 'assets/img/EDIT/GabiNor_0325.jpg',
        secondaryUrl: 'assets/img/EDIT/GabiNor_0324.jpg',
        currentUrl: 'assets/img/EDIT/GabiNor_0325.jpg'
    },
    {
        id: 2,
        name: "Anais Delgado",
        principalUrl: 'assets/img/EDIT/AniNor_0283.jpg',
        secondaryUrl: 'assets/img/EDIT/AniNor_0282.jpg',
        currentUrl: 'assets/img/EDIT/AniNor_0283.jpg'
    },
    {
        id: 3,
        name: "Micaela Andino",
        principalUrl: 'assets/img/EDIT/MikaNor_0331.jpg',
        secondaryUrl: 'assets/img/EDIT/MikaNor_0360.jpg',
        currentUrl: 'assets/img/EDIT/MikaNor_0331.jpg'
    },
    {
        id: 4,
        name: "Vanessa Espinosa",
        principalUrl: 'assets/img/EDIT/VaneNor_0299.jpg',
        secondaryUrl: 'assets/img/EDIT/VaneNor_0305.jpg',
        currentUrl: 'assets/img/EDIT/VaneNor_0299.jpg'
    }
]
export let groupPhotos: PhotoModel[] = [
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg'
    },
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0204.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0204.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0204.jpg'
    },
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0108.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0108.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0108.jpg'
    }, 
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0204.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0204.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0204.jpg'
    }
    , 
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/Gru1.1_0108.jpg'
    }
    , 
    {
        id: 1,
        name: "group1",
        principalUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0236.jpg',
        secondaryUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0236.jpg',
        currentUrl: 'assets/img/IND/EDIT/Grupales/GruNor_0236.jpg'
    }

]