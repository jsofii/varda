export class PhotoModel{
    id: number
    name: string
    principalUrl: string
    secondaryUrl: string
    currentUrl: string
}