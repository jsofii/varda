import { Component, OnInit } from '@angular/core';
import { groupPhotos, photoData } from '../typography/typography.data';
import { PhotoModel } from '../typography/typography.models';

@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  photosData: PhotoModel[] = photoData;
  groupPhotos: PhotoModel[] = groupPhotos;
  constructor() {
    this.photosData = photoData
    this.groupPhotos = groupPhotos;
  
   }
   onMouseOver(id: number){
    const index = this.photosData.findIndex(val => val.id === id);
    this.photosData[index].currentUrl = this.photosData[index].secondaryUrl
   }
   onMouseOut(id: number){
    const index = this.photosData.findIndex(val => val.id === id);
    this.photosData[index].currentUrl = this.photosData[index].principalUrl
   }
  ngOnInit() {}
}
